package com.TKF.FamJam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FamJamApplication {

	public static void main(String[] args) {
		SpringApplication.run(FamJamApplication.class, args);
	}

}
